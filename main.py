import argparse
import os
from scrabble_score import best_word
from scrabble_score import score
from scrabble_score import word_is_real
import sys
import time 

def variable_assignment():
    parser=argparse.ArgumentParser()

    parser.add_argument("letters",type=str,
                       help="add letters you have")
  
    parser.add_argument("-m","--multipliertile",
                       type=int,
                       default = 1,
                       choices = [1,2,3],
                       help="choose if you have a multiplier on the word")

    parser.add_argument("-b","--bonus",
                       help="use this to activatebonus letter tiles",
                       action="store_true")

    parser.add_argument("-ml","--multipliedletter",
                       type=str,
                       help="choose the letter that you wish to multiplier")

    parser.add_argument("-lm","--lettermultiplier",
                       type=int,
                       default=1,
                       help="choose by how much the letter are multiplied")

    parser.add_argument("-sml","--secondmultipliedletter",type=str,default='?',
                   help="choose the second letter that you wish to multiplier")

    parser.add_argument("-slm","--secondlettermultiplier",
                       type=int,
                       default=1,
                       help="choose by how much the second letter are multiplied")

    parser.add_argument("-c",'--cheat',
                       help="use if you want to cheat",
                       action="store_true")

    parser.add_argument("-d","--debug",
                       help="adds a timer function to the code",
                       action="store_true")

    return parser.parse_args()

def main(word):
    """Discovers whether or not word is valid for scrabble

    :param word: inputted by the user when starting the file"""
    args=variable_assignment()
    bonus = 0
    answer = score(word)
    if args.bonus:
        bonus+=(score(args.multipliedletter)*\
               (args.lettermultiplier-1)) +\
               (score(args.secondmultipliedletter)*\
               (args.secondlettermultiplier-1))      
    answer = answer*args.multipliertile
    answer += bonus
    if word_is_real(word.lower()):
        print("\"" + word + "\" is worth " + str(answer) + " points")
    else:
        print(str(word.lower()) + ' :This is not a word')

def cheat(word):
    """Cheats for scrabble and finds the best possible word with the letters available

    :param word: inputted by the user when starting the file"""
    args=variable_assignment()
    bonus = 0
    if args.bonus:
        bonus+=(score(args.multipliedletter)*\
               (args.lettermultiplier-1)) +\
               (score(args.secondmultipliedletter)*\
               (args.secondlettermultiplier-1))  
    if '?' in word:
        newword = '' 
        usedword = best_word(word)              
        for i in word:
           if i in usedword:
               newword += i
        answer = score(newword)
        answer += bonus
        answer *= args.multipliertile
        print('Your best option is: ' + (str(best_word(word.lower())) + str(answer)))
    else:
        answer = score(best_word(word.lower()))
        answer += bonus
        answer = answer*args.multipliertile
        print('Your best option is: '+ (str(best_word(word.lower()))+str(answer)))

def starting():
    """Decides whether to use main or cheat code"""

    args=variable_assignment()
    if args.debug:
        start_time=time.time()
    if args.cheat:
        cheat(args.letters)
    else:
        main(args.letters)
    if args.debug:
        print(time.time()-start_time)

if __name__ == "__main__":
    starting()
