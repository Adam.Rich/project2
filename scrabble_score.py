def transform(data):
    """Assigns a numerical value to each letter

    :param data: each letter in the alphabet gets assigned
    :returns: the letters numerical score"""

    return {letter.lower(): score
            for score in data
            for letter in data[score]}

score_to_letter = {
    1: ['A', 'E', 'I', 'O', 'U', 'L', 'N', 'R', 'S', 'T'],
    2: ['D', 'G'],
    3: ['B', 'C', 'M', 'P'],
    4: ['F', 'H', 'V', 'W', 'Y'],
    5: ['K'],
    8: ['J', 'X'],
    10: ['Q', 'Z']}

letter_to_score = transform(score_to_letter)

def score(word):
    """Main funciton for scrabble game

    :param word: is the original word inputted by the user
    :returns: the score for the letters given in word"""

    return sum([letter_to_score.get(letter, 0)
                for letter in str(word).lower()])

def wild_card_count(word):
    """Counts the number of wildcards given by user

    :param word: word inputted when beginning the function
    :returns: the number of wildcards providing it is less than 2"""

    number = word.count('?')
    if number > 2:
        print("You can have a maximum of 2 wildcards")
    else:
        return number

def read_word(word):
    """Checks the word against the dictionary file

    :param word: word inputted when beginning the function
    :returns: True or None depending on if the word inputted is real"""

    with open("words.txt",'r') as read_obj:
        for line in read_obj:        
            index = 0
            while index < len(word):
                if word[index] == line[index]:
                    index += 1
                else:
                    break
            if index == len(word):
                return True

def ask_for_wildcard():
    """Asks the user for what they want the wildcard to be

    :returns: the users choice of letter"""

    inpt=input('Please input the desired letter of your wildcard: ')
    return inpt

def wildcards(word,letter):
    """Changes the original word to the word with the chosen letter for the wildcard

    :param word: gets the original word
    :param letter:states the desired wanted for the wildcard
    :returns: the new word with the adjested wildcard"""

    word=word[:word.index('?')]+letter.lower()+word[(word.index('?') +1 ):]
    return word
            
def word_is_real(word):
    """Checks if a word is a real word

    :param word:the word originally put in by the user
    :returns: whether or not the word is real (True or not)"""

    if '?' in word:
        number_of_wildcards = wild_card_count(word)
        if number_of_wildcards == 1:
            wildcard = ask_for_wildcard()
            return read_word(wildcards(word,wildcard))        
        elif number_of_wildcards == 2:
            wildcard1 = ask_for_wildcard()
            wildcard2 = ask_for_wildcard()
            return read_word(wildcards(wildcards(word,wildcard1),wildcard2))                       
        else:
            print('You can have a max of 2 wildcards')
    else:
        return read_word(word)

def score_calculator(word, new_word):
    """Calculates the score of the word without the wildcard
       
    :param word: the original word inputted, with wildcards
    :param new_word: the word used once the wildcards have been changed to letters
    :returns: score of the letters that are used in new_word with wildcards accounted for """
    
    second_new_word=''
    for i in word:
        if i in new_word:
            second_new_word += i
    return score(second_new_word)

def best_word_search(new_word,word):
    """Searches the dictionary for the best option available

    :param new_word: if there were any wildcards the word has been adjusted to include the correct letter
    :param word: this is the original word used to know how many points the new_word is really worth
    :returns: best possible word for the letters in new_word"""
  
    answer = 0
    best_word = ''
    with open("words.txt","r") as read_obj:
        for line in read_obj:
            number = len(line)
            scores = score_calculator(word, line)
            letter = 1
            for i in str(new_word):
                if (i in line):
                     letter += 1
            if letter == number and scores>int(answer):
                answer = scores
                best_word = str(line)
    return best_word

def best_word(word):
    """Looks for the best word possible

    :param word: the original word inputted by the user when starting the file
    :returns: the best possible word available with the given letters"""

    answer = 0
    best_word = '' 
    alphabet = ['a','b','c','d','e','f','g','h','i','j',
                'k','l','m','n','o','p','q','r','s','t','u','v','x','y','z']
    if '?' in word:
        number_of_wildcards = wild_card_count(word)     
        if number_of_wildcards == 1:
            for letters in alphabet:         
                new_word = wildcards(word,letters)
                best_word = best_word_search(new_word,word)
                scores = score_calculator(word, best_word)
                if answer < scores:
                    good_word = best_word
                    answer = scores
            return good_word
        elif number_of_wildcards == 2:        
            for letters in alphabet:
                new_word = wildcards(word,letters)
                for letters in alphabet:
                    new_new_word = wildcards(new_word,letters)
                    best_word = best_word_search(new_new_word,word)
                    scores = score_calculator(word, best_word)
                    if answer < scores:
                        good_word = best_word
                        answer = scores
            return good_word
    else:
        return best_word_search(word,word)


